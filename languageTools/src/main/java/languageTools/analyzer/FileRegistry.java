/**
 * The GOAL Grammar Tools. Copyright (C) 2014 Koen Hindriks.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
 */

package languageTools.analyzer;

import java.io.File;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import languageTools.program.Program;

/**
 * Keeps a record of all files processed by a validator, to avoid duplicate
 * processing. Stores a time stamp and the program that is the result of
 * validating the file with the file.
 */
public class FileRegistry {
	protected Map<File, Program> filePrograms = new ConcurrentHashMap<>();
	protected Map<File, Long> fileTimeStamps = new ConcurrentHashMap<>();

	/**
	 * Registers a file with associated program.
	 *
	 * @param source
	 *            The source file for a program.
	 * @param program
	 *            A program.
	 */
	public void register(File source, Program program) {
		if (source != null && program != null) {
			this.filePrograms.put(source, program);
			this.fileTimeStamps.put(source, source.lastModified());
		}
	}

	/**
	 * Removes the file from the registry
	 */
	public void unregister(File source) {
		this.filePrograms.remove(source);
		this.fileTimeStamps.remove(source);
	}

	/**
	 * @return All source files stored in this registry.
	 */
	public Set<File> getSourceFiles() {
		return Collections.unmodifiableSet(this.filePrograms.keySet());
	}

	/**
	 * @param source
	 *            A source file.
	 * @return A program, if the file is registered, {@code null} otherwise.
	 */
	public Program getProgram(File source) {
		return this.filePrograms.get(source);
	}

	/**
	 * @param source
	 *            A file.
	 * @return {@code true} if file is new or modified since it was last
	 *         registered.
	 */
	public boolean needsProcessing(File source) {
		boolean newfile = !this.fileTimeStamps.containsKey(source);
		boolean modified = false;
		if (!newfile) {
			modified = this.fileTimeStamps.get(source) != source.lastModified();
		}
		return newfile || modified;
	}
}